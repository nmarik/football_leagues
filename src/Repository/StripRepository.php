<?php

namespace App\Repository;

use App\Entity\Strip;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Strip|null find($id, $lockMode = null, $lockVersion = null)
 * @method Strip|null findOneBy(array $criteria, array $orderBy = null)
 * @method Strip[]    findAll()
 * @method Strip[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StripRepository extends ServiceEntityRepository
{
    /**
     * StripRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Strip::class);
    }
}
