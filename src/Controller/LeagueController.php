<?php

namespace App\Controller;

use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/leagues")
 */
class LeagueController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * LeagueController constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("", methods="GET")
     *
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $leagues = [
            'leagues' => [
                $this->getDoctrine()
                    ->getManager()
                    ->getRepository('App:League')
                    ->findAll()
            ]
        ];

        return JsonResponse::fromJsonString($this->serializer->serialize($leagues, 'json'));
    }

    /**
     * @Route("/{id}", methods="GET", defaults={"_format": "json"}, requirements={"id"="\d+"})
     *
     * @param int $id
     * @return JsonResponse
     */
    public function getById(int $id): JsonResponse
    {
        $league = [
            'league' => [
                $this->getDoctrine()
                    ->getManager()
                    ->getRepository('App:League')
                    ->findOneBy(['id' => $id])
            ]
        ];

        return JsonResponse::fromJsonString($this->serializer->serialize($league, 'json'));
    }
}