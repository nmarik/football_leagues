<?php

namespace App\Util;

use Firebase\JWT\JWT;
use stdClass;

class JwtUtil
{
    /**
     * @var string
     */
    private $jwtAlgorithm;

    /**
     * @var string
     */
    private $jwtSecret;

    /**
     * JwtUtil constructor.
     * @param string $jwtAlgorithm
     * @param string $jwtSecret
     */
    public function __construct(
        string $jwtAlgorithm,
        string $jwtSecret
    ) {
        $this->jwtAlgorithm = $jwtAlgorithm;
        $this->jwtSecret = $jwtSecret;
    }

    /**
     * @param iterable $tokenData
     * @return string
     */
    public function encode(iterable $tokenData): string
    {
        return JWT::encode($tokenData, $this->jwtSecret, $this->jwtAlgorithm);
    }

    /**
     * @param string $tokenString
     * @return stdClass
     */
    public function decode(string $tokenString): stdClass
    {
        return JWT::decode($tokenString, $this->jwtSecret, [$this->jwtAlgorithm]);
    }
}